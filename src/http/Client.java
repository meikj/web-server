package http;

import http.logger.Logger;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.util.Map;

/**
 * Represents a way of responding to a connected client in the format of a HTTP
 * packet using a Response object.
 */
public class Client {
	
	private Socket client;
	
	/**
	 * Construct a client using a socket.
	 * 
	 * @param client The connected client socket.
	 */
	public Client(Socket client) {
		this.client = client;
	}
	
	/**
	 * Send a response to the client.
	 * 
	 * @param response The response.
	 */
	public void respond(Response response) {
		try {
			OutputStream cout = client.getOutputStream();
			String statusLine = response.getStatusLine();
			
			Logger.log("-- START RESPONSE --");
			
			// Send status line
			cout.write(statusLine.getBytes());
			Logger.log(statusLine.trim());
			
			// Send headers
			if (response.hasHeaders()) {
				Map<String, String> headers = response.getHeaders();
				for (String h : headers.keySet()) {
					String headerLine = String.format("%s: %s\n", h, headers.get(h));
					cout.write(headerLine.getBytes());
					Logger.log(headerLine.trim());
				}
			}
			
			// Separator
			cout.write("\n".getBytes());
			Logger.log("");
			
			// Send entity data if applicable (string is prioritised over file)
			if (response.hasBody()) {
				cout.write(response.getBody().getBytes());
				Logger.log(response.getBody());
			} else if (response.hasFile()) {
				sendFile(response.getFile());
				Logger.log("Sent file (%s)", response.getFile().getPath());
			}
			
			Logger.log("-- END RESPONSE --");
		} catch (SocketException e) {
			// Client has disconnected from server
		} catch (IOException e) {
			// Something went wrong with writing
		} 
		
		try { client.close(); } catch (Exception e) { };
	}
	
	/**
	 * Retrieve the associated socket.
	 */
	public Socket getSocket() {
		return client;
	}
	
	/**
	 * Send a file to the client.
	 * 
	 * @param f The file.
	 * @throws IOException If either the file read fails or client write fails.
	 */
	private void sendFile(File f) throws IOException {
		DataInputStream dis = new DataInputStream(new FileInputStream(f));
		byte[] buf = new byte[Config.PAGE_BUFFER_SIZE];
		int res;
		
		do {
			if ((res = dis.read(buf, 0, Config.PAGE_BUFFER_SIZE)) == -1)
				break;
			
			client.getOutputStream().write(buf, 0, res);
		} while (res == Config.PAGE_BUFFER_SIZE);
		
		dis.close();
	}

}
