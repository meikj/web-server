package http.logger;

import http.Config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger
{
	public static DateFormat dateFormat = new SimpleDateFormat(
			"dd/MM/yyyy HH:mm:ss");
	public static String date = dateFormat.format(new Date().getTime());
	public static String log = "LOG";
	public static String warn = "WARN";
	public static String error = "ERROR";
	
	public static File logFile;
	private static FileWriter writer;

	static {
		if (Config.LOG_FILE) {
			logFile = new File(Config.LOG_FILE_PATH);
			
			try {
				writer = new FileWriter(logFile, true);
			} catch (IOException e) {
				// Can't open log file so we can't log to file
				Config.setLogFile(false);
			}
		}
	}
	
	public static void log(String s) {
		String output = String.format("[%5s][%s] %s\n", log, date, s);

		if (http.Config.LOG_CONSOLE)
		{
			System.out.print(output);
		}
		if (http.Config.LOG_FILE)
		{
			try
			{
				if (!logFile.exists())
				{
					logFile.createNewFile();
				}
				
				writer.write(output);
				writer.flush();

			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}

		}
	}

	public static void log(String format, Object... arguments)
	{
		String output = String.format(format, arguments);
		log(output);
	}
	
	public static void warn(String s) {
		String output = String.format("[%5s][%s] %s\n", warn, date, s);

		if (http.Config.LOG_CONSOLE)
		{
			System.out.print(output);
		}
		if (http.Config.LOG_FILE)
		{
			try
			{
				if (!logFile.exists())
				{
					logFile.createNewFile();
				}
				
				writer.write(output);
				writer.flush();

			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void warn(String format, Object... arguments)
	{
		String output = String.format(format, arguments);
		warn(output);
	}
	
	public static void error(String s) {
		String output = String.format("[%5s][%s] %s\n", error, date, s);

		if (http.Config.LOG_CONSOLE)
		{
			System.err.print(output);
		}
		if (http.Config.LOG_FILE)
		{
			try
			{
				if (!logFile.exists())
				{
					logFile.createNewFile();
				}
				
				writer.write(output);
				writer.flush();

			}
			catch (UnsupportedEncodingException e)
			{
				e.printStackTrace();
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static void error(String format, Object... arguments)
	{
		String output = String.format(format, arguments);
		error(output);
	}

}
