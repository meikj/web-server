package http;

import http.handlers.RequestHandler;
import http.logger.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Represents a web server. This maintains a fixed thread pool for processing
 * client requests.
 */
public class WebServer {

	public static final String CONFIG_FILE = "/server.properties";

	private ServerSocket server;
	private int port;
	private int maxThreadCount;
	private ExecutorService threadPool;
	private boolean running = false;

	/**
	 * Construct a new web server using default configuration.
	 */
	public WebServer() {
		this.port = Config.PORT;
		this.maxThreadCount = Config.THREAD_COUNT;
	}

	/**
	 * Construct a new web server with a custom configuration.
	 * 
	 * @param port
	 *            The port number to listen on.
	 * @param maxThreadCount
	 *            The maximum number of threads for the pool.
	 */
	public WebServer(int port, int maxThreadCount) {
		this.port = port;
		this.maxThreadCount = maxThreadCount;
	}

	public void start() throws IOException {
		server = new ServerSocket(port);
		threadPool = Executors.newFixedThreadPool(maxThreadCount);
		running = true;

		Logger.log("Web server is listening on port %d", port);
		while (running) {
			Socket client = server.accept();
			Logger.log("Passing client (%s:%d) onto RequestHandler",
					client.getInetAddress(), client.getPort());
			threadPool.execute(new RequestHandler(new Client(client)));
		}
	}

	/**
	 * Stop the web server.
	 */
	public void stop() {
		running = false;

		if (threadPool != null) {
			threadPool.shutdown();
		}

		if (server != null) {
			try {
				server.close();
			} catch (Exception e) {
				// ignore...
			}
		}
	}

	/**
	 * Check if the web server is running.
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Retrieve the port number the web server is listening on.
	 */
	public int getPort() {
		return port;
	}

	/**
	 * Retrieve the maximum thread count of the pool.
	 */
	public int getMaxThreadCount() {
		return maxThreadCount;
	}

	/**
	 * TODO: Perhaps explore use of arguments, e.g. --help, --version, --port,
	 * etc.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			loadConfig();
		} catch (IOException e) {
			Logger.warn("Failure loading config file, using default values instead");
		}

		WebServer ws = new WebServer();

		try {
			ws.start();
		} catch (IOException e) {
			Logger.error(String.format("WebServer IOException: %s", e.getMessage()));
			ws.stop();
		}
	}

	/**
	 * Load the configuration file and update the Config class where applicable.
	 * The configuration file is specified in CONFIG_FILE.
	 * 
	 * @throws IOException
	 *             If an error occurs opening or reading from config file.
	 */
	private static void loadConfig() throws IOException {
		Properties p = new Properties();
		try {
			p.load(WebServer.class.getResourceAsStream(CONFIG_FILE));
		} catch (Exception e) {
			// just skip and use default values
			return;
		}

		if (p.containsKey("port")) {
			Config.setPort(Integer.parseInt(p.getProperty("port")));
		}
		if (p.containsKey("thread_count")) {
			Config.setThreadCount(Integer.parseInt(p
					.getProperty("thread_count")));
		}
		if (p.containsKey("request_buffer_size")) {
			Config.setRequestBufferSize(Integer.parseInt(p
					.getProperty("request_buffer_size")));
		}
		if (p.containsKey("response_buffer_size")) {
			Config.setResponseBufferSize(Integer.parseInt(p
					.getProperty("response_buffer_size")));
		}
		if (p.containsKey("page_buffer_size")) {
			Config.setPageBufferSize(Integer.parseInt(p
					.getProperty("page_buffer_size")));
		}
		if (p.containsKey("error_page_folder")) {
			Config.setErrorPageFolder(p.getProperty("error_page_folder"));
		}
		if (p.containsKey("website_folder")) {
			Config.setWebsiteFolder(p.getProperty("website_folder"));
		}
		if (p.containsKey("php_cgi_folder")) {
			Config.setPhpCgiFolder(p.getProperty("php_cgi_folder"));
		}
		if (p.containsKey("php_cgi_binary")) {
			Config.setPhpCgiBinary(p.getProperty("php_cgi_binary"));
		}
		if (p.containsKey("log_console")) {
			Config.setLogConsole(Boolean.parseBoolean(p.getProperty("log_console")));
		}
		if (p.containsKey("log_file")) {
			Config.setLogFile(Boolean.parseBoolean(p.getProperty("log_file")));
		}
		if (p.containsKey("log_file_path")) {
			Config.setLogFilePath(p.getProperty("log_file_path"));
		}
	}

}
