package http.handlers;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import http.Config;
import http.Request;
import http.Response;
import http.logger.Logger;
import http.php.ScriptExecutor;

/**
 * Represents a HTTP GET method handler.
 */
public class HttpGet implements MethodHandler {
	
	/**
	 * All supported file types and their respective content types.
	 */
	private static final Map<String, String> CONTENT_TYPES;
	static {
		Map<String, String> m = new HashMap<>();

		m.put(".html", "text/html");
		m.put(".htm", "text/html");
		m.put(".css", "text/css");
		m.put(".js", "text/javascript");
		m.put(".jpg", "image/jpeg");
		m.put(".jpeg", "image/jpeg");
		m.put(".gif", "image/gif");
		m.put(".png", "image/png");

		CONTENT_TYPES = Collections.unmodifiableMap(m);
	}

	@Override
	public Response process(Request req) {
		Logger.log("GET handler for: " + req);
		
		Response res = null;
		
		// TODO: Absolute URI and Relative URI
		
		// Attempt to separate query string
		String[] uriSplit = req.getURI().split("\\?", 2);
		String uri = uriSplit[0];
		String queryString = uriSplit.length == 2 ? uriSplit[1] : "";
		
		try {
			// Using canonical file removes dots and represents true file
			File f = new File(Config.WEBSITE_FOLDER + "/" + uri).getCanonicalFile();
			String path = f.getPath();
			
			Logger.log("path = %s", path);
			
			if (path.startsWith(Config.WEBSITE_FOLDER) && f.exists() && f.isFile()) {
				// We have a valid file
				res = new Response(200);
				
				// Check if file is PHP script
				if (path.contains(".php")) {
					String scriptName = path.replace(Config.WEBSITE_FOLDER, "");
					ScriptExecutor se = new ScriptExecutor(scriptName, path, req);
					
					if (!queryString.isEmpty()) {
						se.setQueryString(queryString);
					}
					
					try {
						return se.execute();
					} catch (Exception e) {
						return new Response(500);
					}
				}
				
				// Check extension against supported content types
				for (String ext : CONTENT_TYPES.keySet()) {
					if (path.contains(ext)) {
						res.setHeader("Content-Type", CONTENT_TYPES.get(ext));
					}
				}
				
				// If Content-Type is not set, default to octet stream (binary)
				if (!res.hasHeader("Content-Type")) {
					res.setHeader("Content-Type", "application/octet-stream");
				}
				
				res.setHeader("Content-Length", Long.toString(f.length()));
				res.setFile(f.getPath());
			} else if (path.startsWith(Config.WEBSITE_FOLDER) && f.exists() && f.isDirectory() && uri.endsWith("/")) {
				// Path is a directory, check for index.html/index.htm
				res = new Response(200);
				
				File f2 = new File(path + "/index.html");
				File f3 = new File(path + "/index.htm");
				File f4 = new File(path + "/index.php");
				
				if (f2.exists()) {
					res.setHeader("Content-Type", "text/html");
					res.setHeader("Content-Length", Long.toString(f2.length()));
					res.setFile(f2.getPath());
				} else if (f3.exists()) {
					res.setHeader("Content-Type", "text/html");
					res.setHeader("Content-Length", Long.toString(f3.length()));
					res.setFile(f3.getPath());
				} else if (f4.exists()) {
					String scriptName = f4.getPath().replace(Config.WEBSITE_FOLDER, "");
					ScriptExecutor se = new ScriptExecutor(scriptName, f4.getPath(), req);
					
					try {
						return se.execute();
					} catch (Exception e) {
						return new Response(500);
					}
				} else {
					// TODO: Display file browser
					throw new IOException();
				}
			} else {
				throw new IOException();
			}
		} catch (IOException e) {
			res = new Response(404);
		}
		
		return res;
	}

}
