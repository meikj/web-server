package http.handlers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import http.Client;
import http.Config;
import http.Request;
import http.Response;
import http.logger.Logger;
import http.parser.RequestParser;
import http.parser.ParserException;

/**
 * Represents a way of handling an incoming client request.
 */
public class RequestHandler implements Runnable {

	/**
	 * All supported method types and their respective MethodHandler's.
	 */
	private static final Map<String, Class<? extends MethodHandler>> METHOD_HANDLERS;
	static {
		Map<String, Class<? extends MethodHandler>> m = new HashMap<>();

		// HTTP/1.0 specification supported methods
		m.put("GET", HttpGet.class);
		m.put("POST", HttpPost.class);

		// Add more here if you want to introduce new (custom) methods...

		METHOD_HANDLERS = Collections.unmodifiableMap(m);
	}

	private Client client;
	private RequestParser rp;

	/**
	 * Construct a new request handler with respect to a client.
	 * 
	 * @param client
	 *            The connected client.
	 */
	public RequestHandler(Client client) {
		this.client = client;
		this.rp = new RequestParser(METHOD_HANDLERS.keySet());
	}

	@Override
	public void run() {
		String request = "";

		// Parse request from client
		try {
			request = readRequest();

			if (!request.isEmpty()) {
				processRequest(rp.parse(request));
			}
		} catch (ParserException e) {
			Logger.error("Client request invalid: %s%s", e.getMessage(),
					request);

			// Issue a 400 Bad Request error
			client.respond(new Response(400));
		} catch (IOException e) {
			Logger.error("Client IOException: %s", e.getMessage());

			// Issue a 500 Server Error
			client.respond(new Response(500));
		} catch (Exception e) {
			Logger.error("Client Exception: %s: %s", e, e.getMessage());

			// Issue a 500 Server Error
			client.respond(new Response(500));
		}
	}

	/**
	 * Read the incoming request from the client.
	 * 
	 * @return Returns the full client request as a string.
	 * @throws IOException
	 *             If an IO error occurs with reading from client.
	 */
	private String readRequest() throws IOException {
		BufferedReader bf = new BufferedReader(new InputStreamReader(client
				.getSocket().getInputStream()));
		char[] buf = new char[Config.REQUEST_BUFFER_SIZE];
		StringBuffer request = new StringBuffer();
		int res;

		do {
			if ((res = bf.read(buf, 0, Config.REQUEST_BUFFER_SIZE)) == -1)
				break;

			request.append(buf, 0, res);
		} while (res == Config.REQUEST_BUFFER_SIZE);

		System.out.println("-- BEGIN REQUEST --");
		System.out.println(request.toString());
		System.out.println("-- END REQUEST --");

		return request.toString();
	}

	/**
	 * Process the Request (req) using an appropriate method handler class.
	 * Supported methods and their respective method handler classes are defined
	 * in METHOD_HANDLERS.
	 * 
	 * @precondition req != null && req.method ∈ RequestHandler.METHODS.keySet
	 */
	private void processRequest(Request req) {
		try {
			MethodHandler mh = METHOD_HANDLERS.get(req.getMethod())
					.newInstance();
			client.respond(mh.process(req));
		} catch (InstantiationException e) {
			Logger.error("Error instantiating MethodHandler (%s): %s",
					req.getMethod(), e.getMessage());

			// Issue a 500 Server Error
			client.respond(new Response(500));
		} catch (IllegalAccessException e) {
			Logger.error("IllegalAccessException (%s): %s", req.getMethod(),
					e.getMessage());

			// Issue a 500 Server Error
			client.respond(new Response(500));
		}
	}

}
