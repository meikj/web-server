package http.handlers;

import http.Request;
import http.Response;

/**
 * Represents a way of handling a method.
 */
public interface MethodHandler {
	
	/**
	 * Process the specified Request and return an appropriate Response.
	 * 
	 * @param req The specified Request.
	 * @return Returns the Response.
	 */
	public Response process(Request req);

}
