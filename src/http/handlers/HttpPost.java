package http.handlers;

import java.io.File;
import java.io.IOException;

import http.Config;
import http.Request;
import http.Response;
import http.logger.Logger;
import http.php.ScriptExecutor;

/**
 * Represents a HTTP POST method handler.
 */
public class HttpPost implements MethodHandler {

	@Override
	public Response process(Request req) {
		Logger.log("POST handler for: " + req);
		
		// TODO: Absolute URI and Relative URI
		
		String uri = req.getURI();
		
		try {
			// Using canonical file removes dots and represents true file
			File f = new File(Config.WEBSITE_FOLDER + "/" + uri).getCanonicalFile();
			String path = f.getPath();
			
			Logger.log("path = %s", path);
			
			if (path.startsWith(Config.WEBSITE_FOLDER) && f.exists() && f.isFile()) {
				// We have a valid file
				if (path.contains(".php")) {
					// PHP script
					String scriptName = path.replace(Config.WEBSITE_FOLDER, "");
					ScriptExecutor se = new ScriptExecutor(scriptName, path, req);
					
					try {
						return se.execute();
					} catch (Exception e) {
						return new Response(500);
					}
				} else {
					return new Response(400);
				}
			}
		} catch (IOException e) {
			return new Response(404);
		}
		
		return new Response(500);
	}

}
