package http;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a HTTP response.
 */
public class Response {
	
	/**
	 * Standard HTTP (1.0) status codes mapped to their respective reason phrases.
	 */
	public static final Map<Integer, String> REASON_PHRASES;
	static {
		Map<Integer, String> m = new HashMap<Integer, String>();
		
		m.put(200, "OK");
		m.put(201, "Created");
		m.put(202, "Accepted");
		m.put(204, "No Content");
		m.put(301, "Moved Permanently");
		m.put(302, "Moved Temporarily");
		m.put(304, "Not Modified");
		m.put(400, "Bad Request");
		m.put(401, "Unauthorized");
		m.put(403, "Forbidden");
		m.put(404, "Not Found");
		m.put(500, "Internal Server Error");
		m.put(501, "Not Implemented");
		m.put(502, "Bad Gateway");
		m.put(503, "Service Unavailable");
		
		REASON_PHRASES = Collections.unmodifiableMap(m);
	}
	
	/**
	 * Server error pages.
	 */
	public static final Map<Integer, String> ERROR_PAGES;
	static {
		Map<Integer, String> m = new HashMap<Integer, String>();
		
		m.put(400, Config.ERROR_PAGE_FOLDER + "/" + "400.html");
		m.put(401, Config.ERROR_PAGE_FOLDER + "/" + "401.html");
		m.put(403, Config.ERROR_PAGE_FOLDER + "/" + "403.html");
		m.put(404, Config.ERROR_PAGE_FOLDER + "/" + "404.html");
		m.put(500, Config.ERROR_PAGE_FOLDER + "/" + "500.html");
		m.put(501, Config.ERROR_PAGE_FOLDER + "/" + "501.html");
		m.put(502, Config.ERROR_PAGE_FOLDER + "/" + "502.html");
		m.put(503, Config.ERROR_PAGE_FOLDER + "/" + "503.html");
		
		ERROR_PAGES = Collections.unmodifiableMap(m);
	}
	
	private int statusCode;
	private String reasonPhrase = "N/A";
	private String httpVersion = Config.HTTP_VERSION;
	private Map<String, String> headers;
	private String body;
	private File file;
	
	/**
	 * Construct a new response as per the supplied status code. If the
	 * specified status code matches a standard code, a reason phrase
	 * is implicitly provided, otherwise the reason phrase is "N/A".
	 * If an error page is available, this sets the content of the
	 * response to the page.
	 * 
	 * @param statusCode The status code of the response.
	 */
	public Response(int statusCode) {
		headers = new HashMap<String, String>();
		
		this.statusCode = statusCode;
		if (REASON_PHRASES.containsKey(statusCode)) {
			reasonPhrase = REASON_PHRASES.get(statusCode);
			
			if (ERROR_PAGES.containsKey(statusCode)) {
				file = new File(ERROR_PAGES.get(statusCode));
				setHeader("Content-Type", "text/html");
				setHeader("Content-Length", Long.toString(file.length()));
			}
		}
		
		setDefaultHeaders();
	}
	
	/**
	 * Retrieve the status line of the response.
	 */
	public String getStatusLine() {
		return String.format("%s %d %s%s", httpVersion, statusCode, reasonPhrase, Config.CRLF);
	}
	
	/**
	 * Check that the response contains headers.
	 */
	public boolean hasHeaders() {
		return !headers.isEmpty();
	}
	
	/**
	 * Retrieve the headers of the response.
	 * 
	 * @return The headers.
	 */
	public Map<String, String> getHeaders() {
		return headers;
	}
	
	/**
	 * Check if a specific header exists as part of the response.
	 * 
	 * @param header The header.
	 */
	public boolean hasHeader(String header) {
		for (String k : headers.keySet()) {
			if (k.toLowerCase().equals(header.toLowerCase())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Retrieve the value corresponding to a specific header.
	 * 
	 * @param header The header.
	 * @return The corresponding value.
	 */
	public String getHeader(String header) {
		for (String k : headers.keySet()) {
			if (k.toLowerCase().equals(header.toLowerCase())) {
				return headers.get(k);
			}
		}
		return null;
	}
	
	/**
	 * Set a header field in the response. This will override any field which
	 * exists by the same name. The field is case insensitive,  i.e. 
	 * "user-agent" == "User-Agent".
	 * 
	 * @param header The header.
	 * @param value The corresponding value of the header.
	 */
	public void setHeader(String header, String value) {
		String key = header.trim();
		
		for (String k : headers.keySet()) {
			if (k.toLowerCase().equals(key.toLowerCase())) {
				key = k;
			}
		}
		
		headers.put(key, value);
	}
	
	/**
	 * Set the reason phrase of the response. This is useful when you want to
	 * override the existing reason phrase, or to provide a reason phrase for
	 * a custom/non-standard status code.
	 * 
	 * @param phrase The new reason phrase.
	 */
	public void setReasonPhrase(String phrase) {
		this.reasonPhrase = phrase;
	}
	
	/**
	 * Check if there is a body string set.
	 */
	public boolean hasBody() {
		return body != null;
	}
	
	/**
	 * Retrieve the body string.
	 */
	public String getBody() {
		return body;
	}
	
	/**
	 * Set the body string.
	 */
	public void setBody(String body) {
		this.body = body;
	}
	
	/**
	 * Check if there is a file associated with the response.
	 */
	public boolean hasFile() {
		return file != null;
	}
	
	/**
	 * Set the file pointer in accordance to the specified file path.
	 * 
	 * @param filePath The file path.
	 * @throws IOException If an error occurs opening the file.
	 */
	public void setFile(String filePath) throws IOException {
		this.file = new File(filePath);
	}
	
	/**
	 * Retrieve the file pointer associated with the response.
	 */
	public File getFile() {
		return file;
	}
	
	/**
	 * Set default headers of the response. These are included in *every*
	 * response.
	 */
	private void setDefaultHeaders() {
		setHeader("Server", Config.SERVER);
		setHeader("Connection", "close");
	}
	
}
