package http.parser;

import http.Request;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Represents a way of parsing a client request.
 */
public class RequestParser {

	private String method, uri, version;
	private Map<String, String> headers;
	private String body;
	private Set<String> methods;

	/**
	 * Construct a request parser adhering to a set of methods.
	 * 
	 * @param methods
	 *            The set of supported methods.
	 */
	public RequestParser(Set<String> methods) {
		this.methods = methods;
	}

	/**
	 * Parse the specified input, assuming that the input conforms to the
	 * structure of a HTTP compatible request.
	 * 
	 * @param input
	 *            The input to parse.
	 * @return Returns the constructed Request.
	 * @throws ParserException
	 *             If the input is invalid.
	 */
	public Request parse(String input) throws ParserException {
		headers = new HashMap<String, String>();

		String[] tokens = input.split("(\r\n)");

		if (tokens.length < 1) {
			// Request line required
			throw new ParserException("Request line missing");
		}

		// Parse request line
		String requestLine = tokens[0];
		parseRequestLine(requestLine);

		// Process headers and body
		boolean b = false;
		StringBuffer sbuf = new StringBuffer();
		for (int i = 1; i < tokens.length; i++) {
			if (b) {
				sbuf.append(tokens[i]);
			} else if (tokens[i].isEmpty()) {
				b = true;
			} else {
				parseHeader(tokens[i]);
			}
		}

		if (b) {
			body = sbuf.toString();
			return new Request(method, uri, version, headers, body);
		} else {
			return new Request(method, uri, version, headers);
		}
	}

	/**
	 * Parse the string as a HTTP request line.
	 * 
	 * @param requestLine
	 *            The request line string.
	 * @throws ParserException
	 *             If the request line is invalid.
	 */
	private void parseRequestLine(String requestLine) throws ParserException {
		if (requestLine.trim().isEmpty())
			// Skip if empty...
			return;

		String[] tokens = requestLine.split("\\s");

		if (tokens.length < 3) {
			// Invalid request line -- 3 tokens required
			throw new ParserException("Request line invalid: " + requestLine);
		}

		method = tokens[0].toUpperCase();
		uri = tokens[1];
		version = tokens[2].toUpperCase();

		// Validate (to some extent) request line
		if (!methods.contains(method)) {
			throw new ParserException("Unsupported method type: " + method);
		}
		if (!version.equals("HTTP/1.0") && !version.equals("HTTP/1.1")) {
			throw new ParserException("Unsupported HTTP version: " + version);
		}
	}

	/**
	 * Parse the string as a HTTP header line.
	 * 
	 * @param header
	 *            The header line string.
	 * @throws ParserException
	 *             If the header line is invalid.
	 */
	private void parseHeader(String header) throws ParserException {
		if (header.trim().isEmpty())
			// Skip if empty...
			return;

		String[] tokens = header.split(":", 2);

		if (tokens.length < 2) {
			// We expect at least a field and value
			throw new ParserException("Invalid header: " + header);
		}

		String field = tokens[0].trim();
		String value = tokens[1].trim();

		headers.put(field, value);
	}

}
