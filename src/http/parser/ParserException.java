package http.parser;

/**
 * Represents an exception in the RequestParser.
 */
public class ParserException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ParserException() {
		super();
	}
	
	public ParserException(String message) {
		super(message);
	}

}
