package http;

import java.util.Map;

/**
 * Represents an immutable HTTP request.
 */
public class Request {
	
	// Request line
	private String method;
	private String uri;
	private String version;
	
	// Headers (Authorization, From, If-Modified-Since, Referrer, User-Agent, Date, Pragma)
	private Map<String, String> headers;
	
	// Request body/entity
	private String body;
	
	/**
	 * Construct a bare bones request consisting of a request line.
	 * 
	 * @param method The method of the request.
	 * @param uri The URI Of the request.
	 * @param version The HTTP version string of the request.
	 */
	public Request(String method, String uri, String version) {
		this.method = method;
		this.uri = uri;
		this.version = version;
	}
	
	/**
	 * Construct a request consisting of a request line and headers.
	 * 
	 * @param method The method of the request.
	 * @param uri The URI Of the request.
	 * @param version The HTTP version string of the request.
	 * @param headers A map of headers to values (e.g. "User-Agent" -> "...").
	 */
	public Request(String method, String uri, String version,
			Map<String, String> headers) {
		this.method = method;
		this.uri = uri;
		this.version = version;
		this.headers = headers;
	}
	
	/**
	 * Construct a request consisting of a request line, headers, and a body.
	 * 
	 * @param method The method of the request.
	 * @param uri The URI Of the request.
	 * @param version The HTTP version string of the request.
	 * @param headers A map of headers to values (e.g. "User-Agent" -> "...").
	 * @param body The body/entity of the request.
	 */
	public Request(String method, String uri, String version,
			Map<String, String> headers, String body) {
		this.method = method;
		this.uri = uri;
		this.version = version;
		this.headers = headers;
		this.body = body;
	}
	
	/**
	 * Retrieve the method of the request.
	 */
	public String getMethod() {
		return method;
	}
	
	/**
	 * Retrieve the URI of the request.
	 */
	public String getURI() {
		return uri;
	}
	
	/**
	 * Retrieve the HTTP version string of the request.
	 */
	public String getVersion() {
		return version;
	}
	
	/**
	 * Check if the request contains a specific header.
	 * 
	 * @param header The header.
	 */
	public boolean hasHeader(String header) {
		if (headers != null) {
			for (String k : headers.keySet()) {
				if (k.toLowerCase().equals(header.toLowerCase())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Retrieve the value of a specific header.
	 * 
	 * @param header The header.
	 */
	public String getHeader(String header) {
		if (headers != null) {
			for (String k : headers.keySet()) {
				if (k.toLowerCase().equals(header.toLowerCase())) {
					return headers.get(k);
				}
			}
		}
		return null;
	}
	
	/**
	 * Check if the request has a body.
	 */
	public boolean hasBody() {
		return body != null;
	}
	
	/**
	 * Retrieve the body of the request.
	 */
	public String getBody() {
		return body;
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %s", method, uri, version);
	}

}
