package http.php;

import http.Config;
import http.Request;
import http.Response;
import http.logger.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a way of executing a PHP script and constructing a response using
 * the returned response string
 * 
 * This uses the CGI 1.1 protocol in conjunction with the php-cgi binary. This
 * class only provides a *partial* implementation of the CGI 1.1 protocol, in
 * other words, only the bare minimum of functionality is implemented to allow
 * basic execution of a PHP script supporting GET and POST.
 * 
 * In terms of GET, there is support for QUERY_STRING, which populates $_GET.
 * 
 * In terms of POST, there is support for piping the body contents to php-cgi
 * via stdin, which populates $_POST.
 */
public class ScriptExecutor {

	public static final String GATEWAY_INTERFACE = "CGI/1.1";
	public static final String REDIRECT_STATUS = "200";

	private String scriptName = "";
	private String scriptFilename = "";
	private String queryString = "";
	private Request req;

	private BufferedWriter bw; // stdin
	private BufferedReader br; // stdout

	/**
	 * Construct a new ScriptExecutor using a script file path and request
	 * method.
	 * 
	 * @param scriptName
	 *            The path to script within root directory (represents
	 *            SCRIPT_NAME). For example: "/foo/bar.php".
	 * @param scriptFilename
	 *            The script file name (represents SCRIPT_FILENAME). For
	 *            example, "/var/www/mywebsite/foo/bar.php".
	 * @param requestMethod
	 *            The request method (GET or POST).
	 */
	public ScriptExecutor(String scriptPath, String scriptFilename, Request req) {
		this.scriptName = scriptPath;
		this.scriptFilename = scriptFilename;
		this.req = req;
	}

	/**
	 * Execute the script and return the constructed Response object from the
	 * output of php-cgi.
	 * 
	 * @return Returns the constructed Response object.
	 * @throws IOException
	 *             If an error occurs in the context of child process (php-cgi)
	 *             with writing to stdin or writing to stdout.
	 */
	public Response execute() throws IOException {
		ProcessBuilder pb = new ProcessBuilder(Config.PHP_CGI_BINARY);
		Map<String, String> env = pb.environment();
		env.put("GATEWAY_INTERFACE", GATEWAY_INTERFACE);
		env.put("SCRIPT_FILENAME", scriptFilename);
		env.put("SCRIPT_NAME", scriptName);
		env.put("REQUEST_METHOD", req.getMethod());
		env.put("REDIRECT_STATUS", REDIRECT_STATUS);
		env.put("DOCUMENT_ROOT", Config.WEBSITE_FOLDER);
		env.put("QUERY_STRING", queryString);
		env.put("SERVER_PORT", Integer.toString(Config.PORT));
		if (req.hasBody())
			env.put("CONTENT_LENGTH", Integer.toString(req.getBody().length()));
		if (req.hasHeader("Content-Type"))
			env.put("CONTENT_TYPE", req.getHeader("Content-Type"));
		if (req.hasHeader("Cookie"))
			env.put("HTTP_COOKIE", req.getHeader("Cookie"));
		if (req.hasHeader("Connection"))
			env.put("HTTP_CONNECTION", req.getHeader("Connection"));
		if (req.hasHeader("Keep-Alive"))
			env.put("HTTP_KEEP_ALIVE", req.getHeader("Keep-Alive"));
		if (req.hasHeader("Cache-Control"))
			env.put("HTTP_CACHE_CONTROL", req.getHeader("Cache-Control"));
		if (req.hasHeader("User-Agent"))
			env.put("HTTP_USER_AGENT", req.getHeader("User-Agent"));
		if (req.hasHeader("Accept"))
			env.put("HTTP_ACCEPT", req.getHeader("Accept"));
		if (req.hasHeader("Accept-Language"))
			env.put("HTTP_ACCEPT_LANGUAGE", req.getHeader("Accept-Language"));
		if (req.hasHeader("Accept-Encoding"))
			env.put("HTTP_ACCEPT_ENCODING", req.getHeader("Accept-Encoding"));
		if (req.hasHeader("Referer"))
			env.put("HTTP_REFERER", req.getHeader("Referer"));
		pb.directory(new File(Config.PHP_CGI_FOLDER));

		final Process pr = pb.start();

		// monitor stderr
		new Thread(new Runnable() {
			@Override
			public void run() {
				BufferedReader brerr = new BufferedReader(
						new InputStreamReader(pr.getErrorStream()));
				String line = null;

				try {
					while ((line = brerr.readLine()) != null) {
						Logger.error("%s: %s", scriptFilename, line);
					}
					brerr.close();
				} catch (IOException e) {
					return;
				}
			}
		}).start();

		// child process stdin
		bw = new BufferedWriter(new OutputStreamWriter(pr.getOutputStream()));
		pipe(req.getBody());

		// child process stdout
		br = new BufferedReader(new InputStreamReader(pr.getInputStream()));
		String stdout = result();
		Response res = generateResponse(stdout);

		return res;
	}

	/**
	 * Retrieve the active query string. This will be in the format of:
	 * "key1=value&key2=value&key3=value...".
	 */
	public String getQueryString() {
		return queryString;
	}

	/**
	 * Set the active query string. This should be in the format of:
	 * "key1=value&key2=value&key3=value...".
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	/**
	 * Pipe the POST body to child process stdin. This should only be called if
	 * bw (BufferedWriter) has been initialised with a process stdin stream. The
	 * stdin stream is closed as a result of this method being called.
	 * 
	 * @param s
	 *            The string to pipe. If this is null or empty, we don't write
	 *            anything, but we still close stream.
	 * @throws IOException
	 *             If an error occurs piping (most likely process has died or
	 *             stdin stream closed).
	 */
	private void pipe(String s) throws IOException {
		if (s != null && !s.isEmpty()) {
			bw.write(s);
			bw.flush();
		}
		bw.close();
	}

	/**
	 * Retrieve the resulting response from the child process stdout. This is a
	 * response string. This should only be called if br (BufferedWriter) has
	 * been initialised with process stdout stream. The stdout stream is closed
	 * as a result of this method being called.
	 * 
	 * @return Returns the response string.
	 * @throws IOException
	 *             If an error occurs receiving (most likely process has died or
	 *             stdout stream closed).
	 */
	private String result() throws IOException {
		char[] buf = new char[Config.RESPONSE_BUFFER_SIZE];
		StringBuffer sbuf = new StringBuffer();
		int res;

		do {
			if ((res = br.read(buf, 0, Config.RESPONSE_BUFFER_SIZE)) == -1)
				break;

			sbuf.append(buf, 0, res);
		} while (res == Config.RESPONSE_BUFFER_SIZE);

		br.close();

		return sbuf.toString();
	}

	/**
	 * Construct a Response object using a response string.
	 * 
	 * @param s
	 *            The response string.
	 * @return Returns a Response object.
	 */
	private Response generateResponse(String s) {
		int statusCode = Integer.parseInt(REDIRECT_STATUS);
		Map<String, String> headers = new HashMap<String, String>();
		String[] stoks = s.split("(\r\n)");
		boolean b = false;
		StringBuffer sbuf = new StringBuffer();

		for (int i = 0; i < stoks.length; i++) {
			if (b) {
				// parse as body
				sbuf.append(stoks[i]);
			} else if (stoks[i].isEmpty()) {
				// parse as body for next iterations
				b = true;
			} else {
				// parse as header
				String[] header = stoks[i].split(":", 2);
				headers.put(header[0].trim(), header[1].trim());
			}
		}

		// Construct Response object
		if (headers.containsKey("Status")) {
			// PHP returned a status, extract the status code
			statusCode = Integer.parseInt(headers.get("Status").split(" ")[0]);
			headers.remove("Status");
		}

		Response res = new Response(statusCode);

		if (!headers.isEmpty()) {
			for (String k : headers.keySet()) {
				res.setHeader(k, headers.get(k));
			}
		}

		if (b && statusCode == 200 && !headers.containsKey("Location")) {
			String resBody = sbuf.toString();
			res.setBody(resBody);
			res.setHeader("Content-Length", Integer.toString(resBody.length()));
		}

		return res;
	}

}
