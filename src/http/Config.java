package http;

/**
 * Represents server configuration values, with functionality for overriding
 * default values.
 */
public final class Config {
	
	public static final String HTTP_VERSION = "HTTP/1.0";
	
	public static final String CRLF = "\r\n";
	
	public static final String SERVER = "ACE/Group4";
	
	public static int PORT = 5000;
	
	public static int THREAD_COUNT = 5;
	
	public static int REQUEST_BUFFER_SIZE = 256;
	
	public static int RESPONSE_BUFFER_SIZE = 256;
	
	public static int PAGE_BUFFER_SIZE = 512;
	
	public static String ERROR_PAGE_FOLDER = "/home/john/errors";
	
	public static String WEBSITE_FOLDER = "/home/john/www";
	
	public static String PHP_CGI_FOLDER = "/usr/bin";
	
	public static String PHP_CGI_BINARY = "php-cgi";
	
	public static boolean LOG_CONSOLE = true;
	
	public static boolean LOG_FILE = true;
	
	public static String LOG_FILE_PATH = "/home/john/server_log.txt";
	
	public static void setPort(int p) {
		PORT = p;
	}
	
	public static void setThreadCount(int n) {
		THREAD_COUNT = n;
	}
	
	public static void setRequestBufferSize(int b) {
		REQUEST_BUFFER_SIZE = b;
	}
	
	public static void setResponseBufferSize(int b) {
		RESPONSE_BUFFER_SIZE = b;
	}
	
	public static void setPageBufferSize(int b) {
		PAGE_BUFFER_SIZE = b;
	}
	
	public static void setErrorPageFolder(String s) {
		ERROR_PAGE_FOLDER = s;
	}
	
	public static void setWebsiteFolder(String s) {
		WEBSITE_FOLDER = s;
	}
	
	public static void setPhpCgiFolder(String s) {
		PHP_CGI_FOLDER = s;
	}
	
	public static void setPhpCgiBinary(String s) {
		PHP_CGI_BINARY = s;
	}
	
	public static void setLogConsole(boolean b) {
		LOG_CONSOLE = b;
	}
	
	public static void setLogFile(boolean b) {
		LOG_FILE = b;
	}
	
	public static void setLogFilePath(String s) {
		LOG_FILE_PATH = s;
	}

}
